import { Http } from '@api/http';
import Router from 'next/router';
import cookie from 'cookie';
import Cookies from 'js-cookie';

import { createRedirect } from '@api/redirects-config';

const isDev = process.env.NODE_ENV !== 'production';
const apiBaseUrl = process.env.API_BASE_URL || (isDev ? 'https://localhost:3000' : 'https://prod-url');

function universalRedirect(res, location) {
  if (res) {
    createRedirect(res, location);
  } else {
    Router.push(location);
  }
}

function configureHttp(req, res) {
  // configure the http client
  Http.configure((client) => {
    client.defaults.baseUrl = apiBaseUrl;

    client.defaults.interceptors[404] = () => {
      Router.push('/not-found');
    };

    client.defaults.interceptors[401] = () => {
      if (!req) {
        Cookies.remove('token');
      }
      universalRedirect(res, '/login');
    };

    client.defaults.interceptors[403] = () => {
      if (!req) {
        Cookies.remove('token');
      }
      universalRedirect(res, '/login');
    };

    client.defaults.interceptors[500] = () => {
      Router.push('/server-error');
    };

    client.onFail = () => {
      universalRedirect(res, '/server-error');
    };

    client.defaults.headers['Content-Type'] = 'application/json';

    // fix Node "unable to verify the first certificate" errors in dev
    // (prod should be fine because it'll have a real SSL cert)
    if (isDev && req) {
      const https = require("https");
      client.defaults.options['agent'] = new https.Agent({ rejectUnauthorized: false });
    }
  });

  Http.setAuth((client) => {
    // set auth header from the token cookie
    const cookieSource = req ? (req.headers.cookie || '') : document.cookie;
    const cookies = cookie.parse(cookieSource);
    const token = cookies.token;

    if (token) {
      client.defaults.headers['Authorization'] = `Bearer ${token}`;
    } else {
      delete client.defaults.headers['Authorization'];
    }
  });
}

export default configureHttp;
