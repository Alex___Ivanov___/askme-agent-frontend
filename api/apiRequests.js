import { http } from '@api/http';

// region Auth Requests

// todo: add api config
const login = credentials => http.post(`/api/auth/signin`, credentials);

// const socialLogin = (provider, data) => http.post(`/api/auth/${provider}`, data);
const socialLogin = (provider, data) => new Promise((resolve) => {
  setTimeout(() => {
    resolve({
      user: {
        name: 'Vasya',
        social: provider,
        data,
      }
    });
  }, 5000);
});

const register = credentials => http.post(`/api/auth/signup`, credentials);

const logout = () => http.post(`/api/auth/signout`);

const getUser = () => http.get(`/api/auth/user`);

export const authApi = { login, socialLogin, register, logout, getUser };
// endregion

export default {
  auth: authApi,
};
