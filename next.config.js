const path = require('path');
const withLess = require('@zeit/next-less');

// fix: prevents error when .less files are required by node
// if (typeof require !== 'undefined') {
//   // eslint-disable-next-line no-unused-vars
//   require.extensions['.less'] = file => { };
// }

module.exports =
  withLess({
    webpack: (config) => {
      // See https://remysharp.com/2019/11/04/nice-imports-with-nextjs
      config.resolve.alias['@api'] = path.resolve(`${__dirname}/api`);
      config.resolve.alias['@lib'] = path.resolve(`${__dirname}/lib`);
      config.resolve.alias['@lang'] = path.resolve(`${__dirname}/locales`);
      config.resolve.alias['@pages'] = path.resolve(`${__dirname}/pages`);
      config.resolve.alias['@public'] = path.resolve(`${__dirname}/public`);
      config.resolve.alias['@styles'] = path.resolve(`${__dirname}/styles`);
      config.resolve.alias['@redux'] = path.resolve(`${__dirname}/redux`);
      config.resolve.alias['@components'] = path.resolve(`${__dirname}/src/components`);
      config.resolve.alias['@views'] = path.resolve(`${__dirname}/src/views`);

      config.module.rules.push(      {
              test: /\.less$/,
              use: ['less-loader', {
                loader: "less-loader",
                options: {
                  javascriptEnabled: true
                }
              }]
            });
      return config;
    },
  });

// Next.js (v.9.3.5) supports paths in jsconfig.json out of the box (experimental feature)
// todo: remove webpack aliases when Next will support jsconfig paths by default
// https://github.com/zeit/next.js/pull/11293
// module.exports = {
//   experimental: {
//     jsconfigPaths: true,
//   },
// };
