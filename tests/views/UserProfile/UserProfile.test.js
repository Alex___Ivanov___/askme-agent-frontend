import React from 'react';
import { mountWrapper } from '@tests/wrapper';

import UserProfile from '@views/UserProfile/UserProfile';

describe('Home page', () => {
  it('should render without throwing an error', () => {
    const component = mountWrapper(<UserProfile />);
    expect(component.find('div').first().text()).toBe('Parent User profile component');
  });
});
