import React from 'react';
import { shallowWrapper } from '@tests/wrapper';

import Home from '../../src/pages/Home/Home';

describe('Home page', () => {
  const props = {
    count: 21,
    decrement: jest.fn(),
    exampleSaga: jest.fn(),
    reset: jest.fn(),
  };
  it('should render without throwing an error', () => {
    const component = shallowWrapper(<Home { ...props } />);
    expect(component.find('h1.title').text()).toBe('Welcome to Next.js!');
  });
});
