import React from 'react';
import LoginProfileInfo from '@views/LoginPage/LoginProfileInfo';
import LayoutWrap from '@components/LayoutWrap';

const LoginProfile = () => (
  <LayoutWrap
    options={{ hasHeader: false }}
  >
    <LoginProfileInfo/>
  </LayoutWrap>
);

export default LoginProfile;
