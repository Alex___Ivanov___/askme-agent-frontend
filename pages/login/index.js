import React from 'react';
import LoginPage from '@views/LoginPage';
import LayoutWrap from '@components/LayoutWrap';

const IndexPage = () => (
  <LayoutWrap
    options={{ hasHeader: false }}
  >
    <LoginPage/>
  </LayoutWrap>
);

export default IndexPage;
