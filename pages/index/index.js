import React from 'react';
import LayoutWrap from '@components/LayoutWrap';
import Main from '@views/MainPage';

const IndexPage = () => (
  <LayoutWrap>
    <Main/>
  </LayoutWrap>
);

export default IndexPage;
