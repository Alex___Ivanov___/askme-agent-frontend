import React from 'react';
import App from 'next/app';
import { createIntl, createIntlCache, RawIntlProvider } from 'react-intl';
import { Provider } from 'react-redux';
import withRedux from 'next-redux-wrapper';
import withReduxSaga from 'next-redux-saga';

import { getLocale } from '@lib/i18n';
import configureStore from '@redux/store';
import configureHttp from '@api/http-client-config';
import configureRouter, { serverRedirect } from '@api/redirects-config';

// This is optional but highly recommended
// since it prevents memory leak
const cache = createIntlCache();

class $App extends App {

  static async getInitialProps({ Component, ctx }) {
    // eslint-disable-next-line no-unused-vars
    const { req, res, store } = ctx;

    if (req) {
      if (serverRedirect(req, res)) {
        return {};
      }
    }

    // get page props
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    // Get the `locale` and `messages` from the request object on the server.
    // In the browser, use the same values that the server serialized.
    const { locale, messages } = req || window.__NEXT_DATA__.props;

    const props = { pageProps, locale, messages };

    if (!props.locale) {
      props.locale = getLocale(req);
    }

    if (!props.messages) {
      const strings = (await import('@lang/strings')).default;
      props.messages = strings[props.locale];
    }

    if (req) {
      // configure the http client (server-only)
      configureHttp(req, res);
      // store.dispatch(authActions.getUser());
    }

    return props;
  }

  // note componentDidMount is client-only and runs once
  componentDidMount() {
    configureHttp();
    configureRouter();
  }

  render() {
    const { Component, pageProps, locale, messages, store } = this.props;

    const intl = createIntl(
      {
        locale,
        messages
      },
      cache
    );

    return (
      <Provider store={store}>
        <RawIntlProvider value={intl}>
          <Component {...pageProps} />
        </RawIntlProvider>
      </Provider>
    );
  }
}

export default withRedux(configureStore)(withReduxSaga($App));
