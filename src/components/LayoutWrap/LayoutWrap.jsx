import React from 'react';
import PropTypes from 'prop-types';
import { Layout } from 'antd';
import Link from 'next/link';
import Head from 'next/head';
import '@styles/shortcodes.less';
import '@styles/antd.less';
import './LayoutWrap.less';

const { Header, Content } = Layout;

const LayoutWrap = ({ children, options }) => {
  const { hasHeader } = options;
  return (
    <>
      <Head>
        <title>{'Home'}</title>
        <link rel="icon" href={'/favicon.ico'}/>
      </Head>
      <Layout className="am-layout">
        {hasHeader && <Header className="am-layout__header flex items-center justify-end pa3">
          <Link href="/">
            <a className="pa3">{'Home'}</a>
          </Link>
          <Link href={"/login"}>
            <a>{'Profile'}</a>
          </Link>
        </Header> }
        <Content className={`am-layout__content${hasHeader ? ' shift' : ''}`}>
          {children}
        </Content>
      </Layout>
    </>
  );
};

LayoutWrap.propTypes = {
  children: PropTypes.node.isRequired,
  options: PropTypes.shape({
    hasHeader: PropTypes.bool,
  }),
};

LayoutWrap.defaultProps = {
  options: {
    hasHeader: true,
  }
};

export default LayoutWrap;
