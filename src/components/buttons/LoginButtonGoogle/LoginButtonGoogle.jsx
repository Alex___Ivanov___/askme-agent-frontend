import React from 'react';
import PropTypes from 'prop-types';
import { GoogleLogin } from 'react-google-login';
import './LoginButtonGoogle.less';

const LoginButtonGoogle = ({ onSuccess, onFailure }) => {
  return (
    <GoogleLogin
      buttonText="Google"
      clientId="623736583769-qlg46kt2o7gc4kjd2l90nscitf38vl5t.apps.googleusercontent.com"
      render={renderProps => (
        <button
          className={'am-google-login-btn'}
          onClick={renderProps.onClick}
          disabled={renderProps.disabled}
        >
          <img src={'./icons/google.svg'} alt={'Google Logo'}/>
          <span className={'am-google-login-btn__text'}>
            {'Login with Google'}
          </span>
        </button>
      )}
      onSuccess={onSuccess}
      onFailure={onFailure}
      cookiePolicy={'single_host_origin'}
      className={'am-login-google-btn'}
    />
  );
};

LoginButtonGoogle.propTypes = {
  onSuccess: PropTypes.func.isRequired,
  onFailure: PropTypes.func.isRequired,
};

export default LoginButtonGoogle;
