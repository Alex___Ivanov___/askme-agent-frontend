import React from 'react';
import PropTypes from 'prop-types';
import FacebookLogin from 'react-facebook-login';
import './LoginButtonFacebook.less';

const LoginButtonFacebook = ({ onSuccess, onFailure }) => {
  return (
      <FacebookLogin
        appId="1584471724930668"
        autoLoad={false}
        fields="name,email,picture"
        callback={onSuccess}
        onFailure={onFailure}
        textButton="Login with Facebook"
        cssClass="am-facebook-login-btn"
        icon={<img src={'./icons/facebook.svg'} alt={'Facebook Logo'}/>}
        disableMobileRedirect
      />
  );
};

LoginButtonFacebook.propTypes = {
  onSuccess: PropTypes.func.isRequired,
  onFailure: PropTypes.func.isRequired,
};

export default LoginButtonFacebook;
