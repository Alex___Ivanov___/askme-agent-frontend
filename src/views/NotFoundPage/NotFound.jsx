import React from 'react';
import Head from 'next/head';

export class NotFound extends React.Component {

  render() {
    return (
      <div>
        <Head>
          <title>{'Not found'}</title>
          <link rel="icon" href={"/favicon.ico"} />
        </Head>
        <div>
          <h1>{'Not found!'}</h1>
          <div>
            {'Not found? Well, what were you looking for?'}
          </div>
        </div>
      </div>
    );
  }
}

export default NotFound;
