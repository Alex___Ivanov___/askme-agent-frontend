import React from 'react';
import Head from 'next/head';

export class Error extends React.Component {

  render() {
    return (
      <div>
        <Head>
          <title>{'Error'}</title>
          <link rel="icon" href={"/favicon.ico"} />
        </Head>

        <div>
          <h1>{'Error!'}</h1>
          <div>
            {'Something went wrong'}
          </div>
        </div>
      </div>
    );
  }
}

export default Error;
