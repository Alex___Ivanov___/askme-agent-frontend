import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

const UserProfile = ({ parent }) => (
  <>
    <div>
      <FormattedMessage id="parent" defaultMessage="Parent"/>
      {' User profile component'}
    </div>
    <div>
      {parent}
    </div>
  </>
);

UserProfile.propTypes = {
  parent: PropTypes.string
};
UserProfile.defaultProps = {
  parent: 'def parent'
};

export default UserProfile;
