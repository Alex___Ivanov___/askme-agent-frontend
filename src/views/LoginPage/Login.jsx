import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { authActions } from '@views/LoginPage/authActions';
import { FormattedMessage } from 'react-intl';
import { Space } from 'antd';
import AuthPageWrapper from '@views/LoginPage/AuthPageWrapper';
import LoginButtonFacebook from '@components/buttons/LoginButtonFacebook';
import LoginButtonGoogle from '@components/buttons/LoginButtonGoogle';
import './Login.less';

const Login = ({ socialLogin }) => {
  return (
    <AuthPageWrapper
      contentClass={'am-social-login'}
      step={1}
    >
      <div className={'am-social-login__title flex justify-center'}>
        <FormattedMessage id={'login_title_message'} defaultMessage={'Login in our system with'}/>
      </div>
      <Space direction="vertical" size={34}>
        <LoginButtonGoogle
          onSuccess={(res) => socialLogin('google', res)}
          onFailure={(err) => console.log('\tgoogle fail > ', err)}
        />
        <div className={'am-social-login__divider-line'}>
          <span className={'am-social-login__divider-line-text'}>
            {'or'}
          </span>
        </div>
        <LoginButtonFacebook
          onSuccess={(res) => socialLogin('facebook', res)}
          onFailure={(err) => console.log('\tgoogle fail > ', err)}
        />
      </Space>
    </AuthPageWrapper>
  );
};

Login.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  socialLogin: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  return {
    ...state.auth
  };
};

const mapDispatchToProps = {
  socialLogin: authActions.socialLogin,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
