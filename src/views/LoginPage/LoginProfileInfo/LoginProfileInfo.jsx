import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Input } from 'antd';
import { authActions } from '@views/LoginPage/authActions';
import { FormattedMessage } from 'react-intl';
import AuthPageWrapper from '@views/LoginPage/AuthPageWrapper';
import '../Login.less';

const LoginProfileInfo = () => {
  return (
    <AuthPageWrapper
      contentClass={'am-login-profile-info'}
      step={2}
    >
      <div className={'am-login-profile-info__input-label flex justify-start'}>
        <FormattedMessage id={'profile_info_your_name'} defaultMessage={'Your name'}/>
      </div>
      <Input
        className={'am-login-profile-info__input'}
        defaultValue={'Vasya Pupkin'}
        placeholder={'Enter your name'}
      />

      <div className={'am-login-profile-info__checkbox flex items-start'}>
        <input
          type={'checkbox'}
          className={'am-login-profile-info__checkbox-input'}
        />
        <span
          className={'am-login-profile-info__checkbox-label'}
        >
          {'By continuing I agree to the Terms of Service and Privacy Policy.'}
        </span>
      </div>

      <Button
        className={'am-login-profile-info__button'}
        type={'primary'}
      >
        {'Register'}
      </Button>
    </AuthPageWrapper>
  );
};

LoginProfileInfo.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  socialLogin: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  return {
    ...state.auth
  };
};

const mapDispatchToProps = {
  socialLogin: authActions.socialLogin,
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginProfileInfo);
