export const authActionTypes = {
  LOGIN_SAGA: '@auth/LOGIN_SAGA',
  SOCIAL_LOGIN_SAGA: '@auth/SOCIAL_LOGIN_SAGA',
  REGISTER_SAGA: '@auth/REGISTER_SAGA',
  LOGOUT_SAGA: '@auth/LOGOUT_SAGA',
  GET_USER_SAGA: '@auth/GET_USER_SAGA',
  SET_USER: '@auth/SET_USER',
  AUTH_ERROR: '@auth/AUTH_ERROR',
};

export const authActions = {
  login: credentials => ({
    type: authActionTypes.LOGIN_SAGA,
    credentials
  }),
  socialLogin: (provider, data) => ({ // TODO: specify data
    type: authActionTypes.SOCIAL_LOGIN_SAGA,
    payload: {
      provider,
      data,
    }
  }),
  register: credentials => ({
    type: authActionTypes.REGISTER_SAGA,
    credentials
  }),
  logout: () => ({ type: authActionTypes.LOGOUT_SAGA }),
  getUser: () => ({ type: authActionTypes.GET_USER_SAGA }),

  setUser: user => ({
    type: authActionTypes.SET_USER,
    user
  }),
  setAuthError: authError => ({
    type: authActionTypes.AUTH_ERROR,
    authError
  })
};
