import { authActionTypes as type, appActionTypes } from '@redux/actions';

export const authInitialState = {
  user: {},
  authError: false,
  isLoading: false,
};

export function authReducer(state = authInitialState, action) {

  switch (action.type) {
    case type.SET_USER:
      return {
        ...state,
        ...{
          user: action.user
        }
      };

    case type.AUTH_ERROR:
      return {
        ...state,
        ...{
          authError: action.authError
        }
      };

    case appActionTypes.SET_LOADING: {
      const { appModule, isLoading } = action.payload;
      if (appModule === '@auth') {
        return {  ...state, isLoading };
      }
      break;
    }

    default:
      return state;
  }
}
