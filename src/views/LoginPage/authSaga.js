import { call, put, takeLatest } from 'redux-saga/effects';
import Router from 'next/router';
import Cookies from 'js-cookie';
import moment from 'moment';
import { wrap } from '@redux/app/sagas';
import { http } from '@api/http';
import { authApi } from '@api/apiRequests';

import { authActions, authActionTypes as actionType } from '@redux/actions';

export function * loginSaga(action) {
  yield put(authActions.setAuthError(false));
  const response = yield call([authApi, 'login'], action.credentials); // see https://github.com/redux-saga/redux-saga/issues/1389

  const data = yield response.json();

  if (data.token) {
    // set the cookie
    const expires = moment(data.expires).diff(moment(), 'days', true);
    Cookies.set('token', data.token, { expires });
    // set the Authorization header
    http.defaults.headers['Authorization'] = `Bearer ${data.token}`;

    // update state and navigate
    yield put(authActions.setUser(data));
    yield call(Router.push, '/');
  } else {
    yield put(authActions.setAuthError(true));
  }
}

export function * socialLoginSaga(action) {
  yield put(authActions.setAuthError(false));
  const { provider, data } = action.payload;

  const { user } = yield call([authApi, 'socialLogin'], provider, data);

  console.log('\tsocialLoginSaga > ', user);
  // update state and navigate
  yield put(authActions.setUser(user));
  yield call(Router.push, '/');
}

export function * registerSaga(action) {
  yield put(authActions.setAuthError(false));
  let response;
  try {
    response = yield call([authApi, 'register'], action.credentials);
  } catch {
    yield put(authActions.setAuthError(true));
    return;
  }

  const data = yield response.json();

  if (data.token) {
    // set the cookie
    const expires = moment(data.expires).diff(moment(), 'days', true);
    Cookies.set('token', data.token, { expires });

    // set the Authorization header
    http.defaults.headers['Authorization'] = `Bearer ${data.token}`;

    // update state and navigate
    yield put(authActions.setUser(data));
    yield call(Router.push, '/');
  } else {
    yield put(authActions.setAuthError(true));
  }
}

export function * logoutSaga() {
  // remove the cookie
  Cookies.remove('token');

  // clear state and navigate
  yield put(authActions.setUser({}));
  yield call(Router.push, '/login');

  try
  {
    // logout from the server
    yield call([authApi, 'logout']);
  } catch {
    console.log('\tlogoutSaga: error'); // todo: remove it
  }
  // remove the Authorization header
  delete http.defaults.headers['Authorization'];
}

export function * getUserSaga() {
  const response = yield call([authApi, 'getUser']);

  const data = yield response.json();

  yield put(authActions.setUser(data));
}

export const authSagas = [
  takeLatest(actionType.LOGIN_SAGA, wrap(loginSaga)),
  takeLatest(actionType.SOCIAL_LOGIN_SAGA, wrap(socialLoginSaga)),
  takeLatest(actionType.REGISTER_SAGA, wrap(registerSaga)),
  takeLatest(actionType.LOGOUT_SAGA, wrap(logoutSaga)),
  takeLatest(actionType.GET_USER_SAGA, wrap(getUserSaga)),
];
