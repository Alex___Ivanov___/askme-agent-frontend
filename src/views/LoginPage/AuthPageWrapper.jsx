import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';
import './Login.less';

const AuthWrapper = ({ children, step, contentClass }) => {
  return (
    <div className={'am-login flex flex-column items-center justify-around'}>
      <div className={'am-login__app-name flex items-center justify-center'}>
        <img src={'../icons/askme.svg'} alt={'App Logo'}/>
      </div>
      <div className={classes('am-login__content flex flex-column', { [contentClass]: Boolean(contentClass) })}>
        {children}
      </div>
      <div className={'am-login__progressbar flex'}>
        <div className={classes('am-login__step', { active: step === 1 })}/>
        <div className={classes('am-login__step', { active: step === 2 })}/>
        <div className={classes('am-login__step', { active: step === 3 })}/>
      </div>
    </div>
  );
};

AuthWrapper.propTypes = {
  children: PropTypes.node.isRequired,
  contentClass: PropTypes.string,
  step: PropTypes.oneOf([1, 2, 3]),
};

AuthWrapper.defaultProps = {
  contentClass: '',
  step: 1,
};

export default AuthWrapper;
