module.exports = {
  "presets": ["next/babel"],
  "env": {
    "development": {
      "plugins": [
        ["react-intl", {
          "messagesDir": "locales/.messages/"
        }]
      ]
    },
    "production": {
      "plugins": [
        ["react-intl", {
          "messagesDir": "locales/.messages/"
        }]
      ]
    }
  }
};
