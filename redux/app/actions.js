export const appActionTypes = {
  SET_LOADING: '@app/SET_LOADING',
  SET_ERROR: '@app/SET_ERROR',
  SHOW_MODAL: '@app/SHOW_MODAL',
  HIDE_MODAL: '@app/HIDE_MODAL',
};

export const appActions = {
  setLoading: (actionType, isLoading) => {
    const appModule = actionType.split('/')[0];
    return { type: appActionTypes.SET_LOADING, payload: { appModule, isLoading }};
    },
  showModal: (modalTitle, modalContent) => ({ type: 'SHOW_MODAL', modalTitle, modalContent }),
  hideModal: () => ({ type: 'HIDE_MODAL' }),
  setError: (error) => ({ type: 'SET_APP_ERROR', error })
};
