import { put } from 'redux-saga/effects';

import { appActions } from '@redux/actions';

export function wrap(saga) {
  return function * (action) {
    yield put(appActions.setError(action.type, false));
    yield put(appActions.setLoading(action.type, true));
    try {
      yield saga(action);
    } catch (e) {
      yield put(appActions.setError(action.type, true));
      console.error(e);
    }
    yield put(appActions.setLoading(action.type, false));
  };
}
