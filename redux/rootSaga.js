import { all } from 'redux-saga/effects';

import { authSagas } from '@views/LoginPage/authSaga';

function * rootSaga () {
  yield all([
    ...authSagas,
  ]);
}

export default rootSaga;
