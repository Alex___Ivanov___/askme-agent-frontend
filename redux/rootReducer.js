import { combineReducers } from 'redux';

import { authReducer,authInitialState } from '@views/LoginPage/authReducer';

export const initialState = {
  auth: authInitialState,
};

export const rootReducer = () => combineReducers({
  auth: authReducer,
});
