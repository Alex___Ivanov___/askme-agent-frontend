module.exports = {
  setupFiles: [
    '<rootDir>/jest.setup.js',
  ],
  testPathIgnorePatterns: [
    '<rootDir>/.next/',
    '<rootDir>/node_modules/',
  ],
  moduleNameMapper: {
    '@api(.*)$': '<rootDir>/api/$1',
    '@public(.*)$': '<rootDir>/public/$1',
    '@slyles(.*)$': '<rootDir>/styles/$1',
    "@lang(.*)$": "<rootDir>/locales/$1",
    "@lib(.*)$": "<rootDir>/lib/$1",
    "@redux(.*)$": "<rootDir>/redux/$1",
    '@components(.*)$': '<rootDir>/src/components/$1',
    '@pages(.*)$': '<rootDir>/pages/$1',
    '@views(.*)$': '<rootDir>/src/views/$1',
    "@tests(.*)$": "<rootDir>/tests/$1",
  },
  coverageThreshold: {
    global: {
      statements: -10, // maximum 10 un-covered statements
      branches: 80,
      functions: 80,
      lines: 80,
    },
  },
  collectCoverageFrom: [
    '<rootDir>/pages/**/*.js',
    '<rootDir>/src/**/*.js',
  ],
  coveragePathIgnorePatterns: [
    '<rootDir>/node_modules/',
    '<rootDir>/api/',
    '<rootDir>/src/pages/_app.js',
    '<rootDir>/src/pages/_document.js',
    '<rootDir>/tests/wrapper.js',
  ],
};
