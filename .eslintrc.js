module.exports = {
  env: {
    'browser': true,
    'es6': true,
    'node': true
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:jsx-a11y/recommended'
  ],
  parser: 'babel-eslint',
  parserOptions: {
    'ecmaFeatures': {
      'jsx': true
    },
    'ecmaVersion': 2018,
    'sourceType': 'module',
    'allowImportExportEverywhere': true
  },
  plugins: [
    'react',
    'jsx-a11y',
    'formatjs'
  ],
  settings: {
    'react': {
      'version': 'detect'
    }
  },
  // couldn't get rule overrides to work for spec files
  // wanted no-undef off for *.test.js
  globals: {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly',
    'describe': 'readonly',
    'it': 'readonly',
    'expect': 'readonly',
    'beforeEach': 'readonly',
    'afterEach': 'readonly',
    'spyOn': 'readonly',
    'jest': 'readonly'
  },
  ignorePatterns: [
    'node_modules/',
  ],
  rules: {
    'semi': [ 'error', 'always' ],
    'react/jsx-no-literals': 'error',
    'formatjs/enforce-placeholders': 'error',
    'formatjs/blacklist-elements': 'error',
    'formatjs/enforce-default-message': 'error',
    'formatjs/enforce-plural-rules': 'error',
    'formatjs/no-camel-case': 'error',
    'formatjs/no-emoji': 'error',
    'formatjs/no-multiple-plurals': 'error',
    'formatjs/no-offset': 'error',
    'formatjs/supported-datetime-skeleton': 'error',
    'jsx-a11y/anchor-is-valid': 'off' // the Next.js <Link><a /></Link> stuff screws this up
  }
};
