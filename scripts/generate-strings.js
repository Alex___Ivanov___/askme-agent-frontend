const { readFileSync, writeFileSync } = require('fs');
const { basename, resolve } = require('path');
const glob = require('glob');

const languageFilenames = glob.sync('./locales/*.json');

// build a structure like
// {
//   "en": {
//     "id": "string"
//   },
//   "ru": {
//     "id": "string"
//   },
//   ...
// }
let data = {};
for (let filename of languageFilenames) {
  let locale = basename(filename, '.json');
  let file = readFileSync(filename, 'utf8');
  data[locale] = JSON.parse(file);
}

let fileContents = `export default ${JSON.stringify(data)}`;

writeFileSync('./locales/strings.js', fileContents);
console.log(`> Wrote strings to: "${resolve('./locales/strings.js')}"`);
